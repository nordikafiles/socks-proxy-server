socks = require('socksv5')
base64 = require('base-64')

global.CONFIG = require './config.coffee'

users = require './data/users.coffee'

for user in users
  if user.base64
    user.password = base64.decode user.password

checkUser = (username, password) ->
  for user in users
    if user.username == username && user.password == password
      return true
  false

srv = socks.createServer (info, accept, deny) ->
  do accept
srv.listen CONFIG.port, 'localhost', ->
  console.log "SOCKS server listening on port #{CONFIG.port}"
srv.useAuth socks.auth.UserPassword (user, password, cb) ->
  cb checkUser user, password
